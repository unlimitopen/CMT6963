﻿/*
 * CMT69632.c
 *
 * Created: 01.07.2016 09:52:59
 *  Author: Christian Möllers
 */ 

#include <avr/io.h>

#include "../CMGlobal.h"

#include <util/delay.h>
#include <stdlib.h>
#include <string.h>

// self made modules 
#include "CMT6963.h"

// The correct power up sequence would be:
// 1. Apply power to MPU and T6963C. The display and the display’s power (VEE) should be off.
// 2. Reset T6963C and wait until it is fully initialised. (5-6 clock cycles).
// 3. Apply power to the display (VEE on), for example with the T6963C DSPON pin
// 4. Apply signals to the display and switch it on (for example with the /DISPOFF pin of the display).
// 5. Normal operating parameters are reached.
// Notes
// 1. The states of the T6963C registers after a “reset” or “halt” are in the T6963C datasheet.
// 2. For powering off, the reverse applies, i.e. first, switch off the display, remove signals,
// remove VEE display power and then switch off the MPU and T6963C (with proper delays, too).

/**
	@brief	none
	@param 	none
	@return	none
*/
void resetT6963(void)
{
	T6963_RE_LOW;
	_delay_ms(50);
	T6963_RE_HI;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void waitT6963(void)
{
	
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void statusT6963(void)
{		
	T6963DBDDR &=~ (1<<T6963DB0) | (1<<T6963DB1) | (1<<T6963DB2) | (1<<T6963DB3) | (1<<T6963DB4) | (1<<T6963DB5) | (1<<T6963DB6) | (1<<T6963DB7);
	T6963_CD_HI;
	T6963_RD_LOW;
	T6963_WR_HI;
	T6963_CE_LOW;

	while(! ( PINA&0x03 ) );

	T6963_CE_HI;
	T6963_RD_HI;
	T6963DBDDR |= (1<<T6963DB0) | (1<<T6963DB1) | (1<<T6963DB2) | (1<<T6963DB3) | (1<<T6963DB4) | (1<<T6963DB5) | (1<<T6963DB6) | (1<<T6963DB7);
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint16_t readT6963(void)
{
	uint16_t temp = 0;
	
	//T6963DBPORT |= (1<<T6963DB0) | (1<<T6963DB1) | (1<<T6963DB2) | (1<<T6963DB3) | (1<<T6963DB4) | (1<<T6963DB5) | (1<<T6963DB6) | (1<<T6963DB7);
	//_delay_ms(1);
	//T6963DBDDR &=~ (1<<T6963DB0) | (1<<T6963DB1) | (1<<T6963DB2) | (1<<T6963DB3) | (1<<T6963DB4) | (1<<T6963DB5) | (1<<T6963DB6) | (1<<T6963DB7);
	//temp = T6963DBPORT;
	//T6963DBPORT |= (1<<T6963DB0) | (1<<T6963DB1) | (1<<T6963DB2) | (1<<T6963DB3) | (1<<T6963DB4) | (1<<T6963DB5) | (1<<T6963DB6) | (1<<T6963DB7);
	//_delay_ms(1);
		
	return temp;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t writeCmdT6963(unsigned char cmd)
{
	uint8_t success = FALSE;
	statusT6963();
	
	T6963DBPORT = cmd;
	T6963_CD_HI;
	T6963_WR_LOW;
	T6963_RD_HI;
	T6963_CE_LOW;
	_delay_us(10);
	T6963_CE_HI;
	
	return success;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t writeDataT6963(unsigned char data)
{
	uint8_t success = FALSE;
	
	statusT6963();
	T6963DBPORT = data;
	T6963_CD_LOW;
	T6963_WR_LOW;
	T6963_RD_HI;
	T6963_CE_LOW;
	_delay_us(10);
	T6963_CE_HI;
	
	return success;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t initT6963(void)
{
	//int status = 0;
	T6963DBDDR &=~ (1<<T6963DB0) | (1<<T6963DB1) | (1<<T6963DB2) | (1<<T6963DB3) | (1<<T6963DB4) | (1<<T6963DB5) | (1<<T6963DB6) | (1<<T6963DB7);
	//T6963DBDDR = 0xFF;

	T6963_RE_OUTPUT;
	T6963_WR_OUTPUT;
	T6963_RD_OUTPUT;
	T6963_CE_OUTPUT;
	T6963_CD_OUTPUT;
	
	resetT6963();
	T6963_CE_HI;
	
	writeDataT6963(0x00);
	writeDataT6963(0x00);
	writeCmdT6963(0x42);
	
	writeDataT6963(0x1E);
	writeDataT6963(0x00);
	writeCmdT6963(0x43);

	writeDataT6963(0x00);
	writeDataT6963(0x17);
	writeCmdT6963(0x40);
	
	writeDataT6963(0x1E);
	writeDataT6963(0x00);
	writeCmdT6963(0x41);
	
	writeCmdT6963(0x80);
	writeCmdT6963(0x9C);
	 
	eraseGraphicRam();
	eraseTextRam();
	
	return 0;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void eraseGraphicRam(void)
{
	unsigned int i;

	setRamAddress(0x0000);

	for(i=0; i<0x16FF; i++)
	{
		writeDataT6963(0);
		writeCmdT6963(0xC0);
	}
	setCursorToGlcd(1,1);
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void eraseTextRam(void)
{
	unsigned int i;

	setRamAddress(0x1700);

	for(i=0x1700; i<0xFFFF; i++)
	{
		writeDataT6963(0);
		writeCmdT6963(0xC0);
	}
	setCursorToGlcd(1,1);
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void setRamAddress (unsigned int Adresse)
{
	unsigned char Low;
	unsigned char High;

	Low = Adresse&255;
	Adresse=Adresse>>8;
	High = Adresse&255;
	
	writeDataT6963(Low);
	writeDataT6963(High);
	writeCmdT6963(0x24);
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void writeToGlcd (unsigned char Zeichen)
{
	unsigned char ASCII;

	ASCII = Zeichen-0x20;
	writeDataT6963 	(ASCII);
	writeCmdT6963	(0xC0);
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void writeArrayToGlcd (unsigned char Zeile, unsigned char Spalte, char *Text)
{
	setCursorToGlcd(Zeile,Spalte);

	while (*Text)
	{
		writeToGlcd(*Text);
		Text++;
	}
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void setCursorToGlcd (unsigned int Zeile, unsigned int Spalte)
{
	//Für die Funktion benötigte Variablen
	unsigned int Adresse;

	// Ab hier beginnt der Speicherbereich des Text Displays (Default Wert)
	Adresse=0x1700;

	//Wenn die Zieladresse ausserhalb des Displays liegt, sofort zurück springen
	if ((Zeile>16) || (Spalte>30))
	{
		return;
	}

	//Aufrechnen der Zeile und Spalte auf den Text Default Wert
	Adresse = Adresse + (Spalte-1) + ((Zeile-1)*30);

	//Mit der ausgerechneten Adresse den Befehl ausführen
	setRamAddress(Adresse);
}

/**********************************************************************************************/
// Setzten oder Löschen von einem Pixel (Welches das steht in X und Y)
// In X steht das Pixel in der Waagerechten und in Y das Pixel in der Senkrechten
// Die Variable Show sagt aus ob das Pixel gesetzt(1) oder gelöscht(0) werde soll
/**********************************************************************************************/
void setPixelToGlcd(unsigned int Y, unsigned int X, unsigned char Show)
{
	unsigned int ADR;				// Zur Berechnung der RAM Adresse
	unsigned char Bit;
	unsigned char Puffer;

	ADR = 0;						// Die Grafik Home Adresse

	//Wenn der adressierte Bereich außerhalb des Displays liegt, dann springe sofort zurück!
	if ( (X>240) | (Y>DISPLAY_SIZE_WIDTH) | (X==0) | (Y==0) )
	{
		return;
	}

	//Umrechnen der übermittelten Werte in die RAM Adresse
	ADR = ((Y-1)*30)+((X-1)/8);

	setRamAddress(ADR);
	//Nachfragen ob das Display fertig ist
	statusT6963();

	//Errechnen welches Bit gesetzt werden soll
	Puffer=(X-1)%8;
	Bit=7-Puffer;

	//Übertragen/Ausführen des Befehls
	if(Show)
	{
		writeCmdT6963 (0xF8+Bit);
	}
	else
	{
		writeCmdT6963 (0xF0+Bit);
	}
}

/**********************************************************************************************/
//	Zeichnet ein gefülltes Rechteck von X1, X2, Y1, Y2 (AE=1=abgerundete Ecken)
/**********************************************************************************************/
void writeRectangleFullToGlcd (unsigned char X1,unsigned char X2,unsigned char Y1,unsigned char Y2,unsigned char AE)
{
	unsigned char i;
	unsigned char k;

	for (i=0;i<(Y2-(Y1-1));i++)
	{
		for (k=0;k<(X2-(X1-1));k++)
		{
			setPixelToGlcd(Y1+i,X1+k,Setzen);
		}
	}

	//Wenn AE=1 gewählt wurde werden jetzt die Ecken abgerundet
	if ((AE==1)&((Y2-Y1)>=8))
	{
		for(i=0;i<4;i++)
		{
			setPixelToGlcd(Y1+i,X1,Loeschen); 	//Oben links
			setPixelToGlcd(Y1,X1+i,Loeschen);
			setPixelToGlcd(Y1+i,X2,Loeschen); 	//Oben rechts
			setPixelToGlcd(Y1,X2-i,Loeschen);
			setPixelToGlcd(Y2-i,X1,Loeschen); 	//Unten links
			setPixelToGlcd(Y2,X1+i,Loeschen);
			setPixelToGlcd(Y2-i,X2,Loeschen); 	//Unten rechts
			setPixelToGlcd(Y2,X2-i,Loeschen);
		}
		setPixelToGlcd(Y1+1,X1+1,Loeschen); 	//Oben links
		setPixelToGlcd(Y1+1,X2-1,Loeschen);	//Oben rechts
		setPixelToGlcd(Y2-1,X1+1,Loeschen); 	//Unten links
		setPixelToGlcd(Y2-1,X2-1,Loeschen);	//Unten rechts
	}
}
