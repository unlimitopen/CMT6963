﻿/*
 * CMT6963.h
 *
 * Created: 01.07.2016 09:54:09
 *  Author: Christian Möllers
 */ 

#ifndef CMT6963_H_
#define CMT6963_H_


#ifndef TRUE
#define TRUE							1
#endif

#ifndef FALSE
#define FALSE							0
#endif

#define Setzen		1
#define Loeschen	0
#define Waagerecht	1
#define Senkrecht	0

// Register Settings
#define SET_CURSOR_POINTER				1

// Display Size
#define DISPLAY_SIZE_HEIGHT				128
#define DISPLAY_SIZE_WIDTH				128
#define DISPLAY_FONT_WIDTH				6

#define DISPLAY_TEXT_HOME				0
#define DISPLAY_TEXT_AREA				(DISPLAY_SIZE_HEIGHT / DISPLAY_SIZE_HEIGHT)
#define DISPLAY_OFFSETT_REGISTER		2
#define DISPLAY_TEXT_SIZE				(DISPLAY_TEXT_AREA * DISPLAY_SIZE_WIDTH)

#define DISPLAY_SET_TEXT_HOME_ADDRESS	0x40
#define DISPLAY_SET_TEXT_AREA			0x41
#define DISPLAY_SET_OFFSET_REGISTER		0x22
#define DISPLAY_SET_ADDRESS_POINTER		0x24
#define DISPLAY_MODE					0x90
#define DISPLAY_TEXT_MODE				0x04
#define DISPLAY_MODE_SET				0x83
#define DISPLAY_DATA_WRITE_AND_INCREMENT 0xC0

// D0~D7 (Pins 10-17) Data I/O pins for communications between MPU and T6963C.
#define T6963DBPORT			PORTA
#define T6963DBDDR			DDRA
#define T6963DB0			PA0
#define T6963DB1			PA1
#define T6963DB2			PA2
#define T6963DB3			PA3
#define T6963DB4			PA4
#define T6963DB5			PA5
#define T6963DB6			PA6
#define T6963DB7			PA7

#define T6963STPORT			PORTC
#define T6963STDDR			DDRC

//  T6963  /WR (Pin 18) Data Write. It must be driven “low” to write data to VRAM via the T6963C.
#define T6963_WR_PIN		PC2
#define T6963_WR_PORT		PORTC
#define T6963_WR_PINR		PINC
#define T6963_WR_DIRE		DDRC
#define T6963_WR_OUTPUT		(T6963_WR_DIRE |= (1<<T6963_WR_PIN))
#define T6963_WR_INPUT		(T6963_WR_DIRE &=~ (1<<T6963_WR_PIN))
#define T6963_WR_HI			(T6963_WR_PORT |= (1<<T6963_WR_PIN))
#define T6963_WR_LOW		(T6963_WR_PORT &=~ (1<<T6963_WR_PIN))
#define T6963_WR_TOG		(T6963_WR_PORT ^= (1<<T6963_WR_PIN))
#define T6963_WR_IN			(T6963_WR_PINR&(1<<T6963_WR_PIN) ? 1 : 0 )

//  T6963  /RD (Pin 19) Data Read. It must be driven “low” to read data from VRAM via the T6963C.
#define T6963_RD_PIN		PC3
#define T6963_RD_PORT		PORTC
#define T6963_RD_PINR		PINC
#define T6963_RD_DIRE		DDRC
#define T6963_RD_OUTPUT		(T6963_RD_DIRE |= (1<<T6963_RD_PIN))
#define T6963_RD_INPUT		(T6963_RD_DIRE &=~ (1<<T6963_RD_PIN))
#define T6963_RD_HI			(T6963_RD_PORT |= (1<<T6963_RD_PIN))
#define T6963_RD_LOW		(T6963_RD_PORT &=~ (1<<T6963_RD_PIN))
#define T6963_RD_TOG		(T6963_RD_PORT ^= (1<<T6963_RD_PIN))
#define T6963_RD_IN			(T6963_RD_PINR&(1<<T6963_RD_PIN) ? 1 : 0 )

//  T6963 /CE (Pin 20) Chip Enable. It must be driven “low” while MPU communicates with T6963C.
#define T6963_CE_PIN		PC4
#define T6963_CE_PORT		PORTC
#define T6963_CE_PINR		PINC
#define T6963_CE_DIRE		DDRC
#define T6963_CE_OUTPUT		(T6963_CE_DIRE |= (1<<T6963_CE_PIN))
#define T6963_CE_INPUT		(T6963_CE_DIRE &=~ (1<<T6963_CE_PIN))
#define T6963_CE_HI			(T6963_CE_PORT |= (1<<T6963_CE_PIN))
#define T6963_CE_LOW		(T6963_CE_PORT &=~ (1<<T6963_CE_PIN))
#define T6963_CE_TOG		(T6963_CE_PORT ^= (1<<T6963_CE_PIN))
#define T6963_CE_IN			(T6963_CE_PINR&(1<<T6963_CE_PIN) ? 1 : 0 )

//  T6963  C/D (Pin 21) Command/Write selection. To write or read data, it must be driven “low”. To write a Command or read status, it must be driven “high”.
#define T6963_CD_PIN		PC5
#define T6963_CD_PORT		PORTC
#define T6963_CD_PINR		PINC
#define T6963_CD_DIRE		DDRC
#define T6963_CD_OUTPUT		(T6963_CD_DIRE |= (1<<T6963_CD_PIN))
#define T6963_CD_INPUT		(T6963_CD_DIRE &=~ (1<<T6963_CD_PIN))
#define T6963_CD_HI			(T6963_CD_PORT |= (1<<T6963_CD_PIN))
#define T6963_CD_LOW		(T6963_CD_PORT &=~ (1<<T6963_CD_PIN))
#define T6963_CD_TOG		(T6963_CD_PORT ^= (1<<T6963_CD_PIN))
#define T6963_CD_IN			(T6963_CD_PINR&(1<<T6963_CD_PIN) ? 1 : 0 )

//  T6963  /RE (Pin 16) Reset.
#define T6963_RE_PIN		PC6
#define T6963_RE_PORT		PORTC
#define T6963_RE_PINR		PINC
#define T6963_RE_DIRE		DDRC
#define T6963_RE_OUTPUT		(T6963_RE_DIRE |= (1<<T6963_RE_PIN))
#define T6963_RE_INPUT		(T6963_RE_DIRE &=~ (1<<T6963_RE_PIN))
#define T6963_RE_HI			(T6963_RE_PORT |= (1<<T6963_RE_PIN))
#define T6963_RE_LOW		(T6963_RE_PORT &=~ (1<<T6963_RE_PIN))
#define T6963_RE_TOG		(T6963_RE_PORT ^= (1<<T6963_RE_PIN))
#define T6963_RE_IN			(T6963_RE_PINR&(1<<T6963_RE_PIN) ? 1 : 0 )

/*
Commands Description Execute D7 D6 D5 D4 D3 D2 D1 D0 Time 
Pointer Set 0 0 1 0 0 N2 N1 N0 
			0 0 1 Cursor  Pointer Set 
			0 1 0 Offset Register Set
			1 0 0 Address Pointer Set 
			
Control Word 0 1 0 0 0 0 N1 N0 
Set Commands	 0 0 Text Home Address Set 
			 0 1 Text Area Set  
			1 0 Graphic Home Address Set 
			1 1 Graphic Area Set 
					
Mode Set 1 0 0 0 CG N2 N1 N0
			0 CG ROM Mode 
			1 CG RAM Mode 
			0 0 0 "OR" Mode 
			0 0 1 "EXOR" Mode 
			0 1 1 "AND" Mode 
			1 0 0 Text only (attribute capability) 
			
Display Modes 1 0 0 1 N3 N2 N1 N0
			0 Graphics Off 
			1 Graphics On 
			0 Text Off 
			1 Text On 
			0 Cursor Off 
			1 Cursor On 
			0 Cursor blink Off 
			1 Cursor blink On 
			
Cursor  Pattern 1 0 1 0 0 N2 N1 N0 N2~N0: No. of lines for cursor +1
			Select 0 0 0 Bottom Line cursor 
			0 0 1 2 line cursor 
			| | | 
			1 1 1 8 line cursor (block cursor) 
			
Data Auto 1 1 0 0 0 0 N1 N0
			Read/Write 0 0 Data Auto Write Set 
			0 1 Data Auto Read Set 
			1 0 Auto reset (Address pointer autoincremented) for continuous rd/wr 
			
Data Read/Write 1 1 0 0 0 N2 N1 N0
			0 Address Pointer up/down 
			1 Address Pointer unchanged 
			0 Address Pointer up 
			1 Address Pointer down 
			0 Data Write 
			1 Data Read 

Screen Peeking 1 1 1 0 0 0 0 0 Read Displayed Data
Screen Copy (Note 3) 1 1 1 0 1 0 0 0 Copies 1 line of displayed data whose address is indicated by the Address Pointer to Graphic RAM area

Bit Set/Reset 1 1 1 1 N3 N2 N1 N0 N2~N0 indicates the bit in the pointed address
			0 Bit Reset 
			1 Bit Set 
			0 0 0 Bit 0 (LSB) 
			0 0 1 Bit 1 
			| | 
			1 1 1 Bit 7 (MSB)
*/



// function
void resetT6963(void);
void waitT6963(void);
void statusT6963(void);
uint16_t readT6963(void);
uint8_t sendCmdT6963(uint8_t *cmd);
uint8_t sendDataT6963(uint8_t *data);
uint8_t initT6963(void);
void eraseGraphicRam(void);
void eraseTextRam(void);
void setRamAddress (unsigned int Adresse);
void writeToGlcd (unsigned char Zeichen);
void writeArrayToGlcd (unsigned char Zeile, unsigned char Spalte, char *Text);
void setCursorToGlcd (unsigned int Zeile, unsigned int Spalte);
void writeRectangleFullToGlcd (unsigned char X1,unsigned char X2,unsigned char Y1,unsigned char Y2,unsigned char AE);
#endif /* CMT6963_H_ */